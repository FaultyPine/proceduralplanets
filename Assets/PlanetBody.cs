using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
[RequireComponent(typeof(Rigidbody))]
public class PlanetBody : MonoBehaviour
{
    public float surfaceGravity;
    public Vector3 initialVelocity;
    public float radius;
    Transform meshHolder;

    public Vector3 velocity { get; private set; }
    public float mass { get; private set; }
    Rigidbody rb;
    Planet planet;

    public Vector3 Position { get { return rb.position; } }

    private void Awake() {
        SetMass();
        rb = GetComponent<Rigidbody>();
        if (rb == null) {
            rb = transform.gameObject.AddComponent<Rigidbody>();
        }
        planet = GetComponent<Planet>();
        if (planet == null) {
            planet = transform.gameObject.AddComponent<Planet>();
        }
        rb.interpolation = RigidbodyInterpolation.Interpolate;
        rb.isKinematic = true;
        rb.useGravity = false;
        //radius = planet.shapeSettings.planetRadius;
        rb.mass = mass;
        velocity = initialVelocity;
    }

    // update velocity for this planet based on all other planet's gravitational pull
    /*public void UpdateVelocity(PlanetBody[] bodies, float timeStep) {
        foreach (var otherBody in bodies) {
            if (otherBody != this) {
                float sqrDst = (otherBody.rb.position - rb.position).sqrMagnitude;
                Vector3 forceDir = (otherBody.rb.position - rb.position).normalized;
                // F = G * ((m1*m2)/(dist^2))
                Vector3 acceleration = forceDir * Globals.gravitationalConstant * otherBody.mass / sqrDst;
                velocity += acceleration * timeStep;
            }
        }
    }*/

    // update our own velocity using just acceleration
    public void UpdateVelocity (Vector3 acceleration, float timeStep) {
        velocity += acceleration * timeStep;
    }
    // timeStep for these funcs is just our fixed physics timestep interval

    // actually move ourself
    public void UpdatePosition (float timeStep) {
        rb.MovePosition (rb.position + velocity * timeStep);
        //Debug.DrawLine(rb.position, rb.position + velocity, Color.red);
    }

    public void UpdateRotation (float timeStep) {
        if (!transform.gameObject.CompareTag("Sun")) {
            rb.transform.Rotate(1.0f * timeStep, 0.5f * timeStep, 0.0f, Space.Self);
        }
    }


    // when changing inspector settings, make sure to refresh internal values
    void OnValidate () {
        SetMass();
        //meshHolder = transform.GetChild (0);
        //meshHolder.localScale = Vector3.one * radius;
    }

    void SetMass() {
        mass = surfaceGravity * radius * radius / Globals.gravitationalConstant;
    }


}
