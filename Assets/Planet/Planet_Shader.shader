Shader "Custom/Planet"
{
    Properties
    {
        [HDR] _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

        _elevationMinMax ("Elevation Min Max", Vector) = (0,0,0,0)
        _planetTexture ("Planet Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 localPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        float4 _elevationMinMax;
        sampler2D _planetTexture;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float invLerp(float from, float to, float value){
            return (value - from) / (to - from);
        }

        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input,o);
            o.localPos = v.vertex.xyz;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float minElevation = _elevationMinMax.x;
            float maxElevation = _elevationMinMax.y;
            float unscaledElevation = IN.uv_MainTex.y;
            float biomePercent = IN.uv_MainTex.x;

            float ocean = invLerp(minElevation, 0, unscaledElevation);
            float land = invLerp(0, maxElevation, unscaledElevation);

            float oceanMask = 1-floor(ocean);

            ocean = lerp(0.0, 0.5, ocean);
            land = lerp(0.5, 1, land);

            float uvX = (ocean * oceanMask) + (land * (1-oceanMask));
            float2 uv = float2(uvX, biomePercent);

            float4 col = tex2D(_planetTexture, uv);

            o.Albedo = col.rgb * _Color;
            o.Smoothness = oceanMask * _Glossiness;
            //o.Metallic = _Metallic;
            o.Alpha = col.a;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
