using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorGenerator
{
    ColorSettings settings;
    Texture2D texture;
    const int textureResolution = 50;
    INoiseFilter biomeNoiseFilter;

    public void UpdateSettings(ColorSettings settings) {
        this.settings = settings;
        if (texture == null || texture.height != settings.biomeColorSettings.biomes.Length) {
            texture = new Texture2D(textureResolution*2/*times 2 to account for ocean*/, settings.biomeColorSettings.biomes.Length, TextureFormat.RGBA32, false); // each row of the texture corresponds to a biome
            texture.wrapMode = TextureWrapMode.Clamp;
        }
        biomeNoiseFilter = NoiseFilterFactory.CreateNoiseFilter(settings.biomeColorSettings.noise);
    }

    public void UpdateElevation(MinMax elevation) {
        settings.planetMaterial.SetVector("_elevationMinMax", new Vector4(elevation.Min, elevation.Max));
    }

    // return 0 if we're in first biome and 1 if in last biome and a blend for the other biomes
    // biomes are split along the latitude of the sphere (hence the use of height here)
    public float BiomePercentFromPoint(Vector3 pointOnUnitSphere) {
        float heightPercent = (pointOnUnitSphere.y + 1) / 2f;
        heightPercent += (biomeNoiseFilter.Evaluate(pointOnUnitSphere) - settings.biomeColorSettings.noiseOffset) * settings.biomeColorSettings.noiseStrength;
        float biomeIndex = 0;
        int numBiomes = settings.biomeColorSettings.biomes.Length;

        float blendRange = settings.biomeColorSettings.blendAmount / 2f + 0.001f;

        for (int i = 0; i < numBiomes; i++) {
            float dst = heightPercent - settings.biomeColorSettings.biomes[i].startHeight;
            float weight = Mathf.InverseLerp(-blendRange, blendRange, dst);
            biomeIndex *= 1 - weight;
            biomeIndex += i * weight;
        }
        return biomeIndex / Mathf.Max(1, (numBiomes - 1));
    }

    public void UpdateColors() {
        Color[] colors = new Color[texture.width * texture.height];
        int colorIdx = 0;

        foreach (var biome in settings.biomeColorSettings.biomes) {
            for (int i = 0; i < textureResolution * 2; i++) {
                Color gradientCol;
                if (i < textureResolution) {
                    gradientCol = settings.oceanColor.Evaluate( i / (textureResolution - 1f));
                }
                else {
                    gradientCol = biome.gradient.Evaluate( (i-textureResolution) / (textureResolution - 1f));
                }
                Color tintColor = biome.tint;
                colors[colorIdx] = gradientCol * (1-biome.tintPercent) + tintColor * biome.tintPercent;
                colorIdx += 1;
            }
        }
        texture.SetPixels(colors);
        texture.Apply();
        settings.planetMaterial.SetTexture("_planetTexture", texture);
    }
}
