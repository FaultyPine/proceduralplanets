using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidNoiseFilter : INoiseFilter
{
    NoiseSettings.RigidNoiseSettings settings;
    Noise noise = new Noise();

    public RigidNoiseFilter(NoiseSettings.RigidNoiseSettings settings) {
        this.settings = settings;
    }

    public float Evaluate(Vector3 point) {
        //float noiseValue = noise.Evaluate(point * settings.roughness + settings.center); // range [-1, 1]
        //noiseValue = (noiseValue+1) * 0.5f; // range [0,1]
        float noiseValue = 0;

        float frequency = settings.baseRoughness;
        float amplitude = 1;
        float weight  = 1;

        for (int i = 0; i < settings.numLayers; i++) {
            float v = 1 - Mathf.Abs(noise.Evaluate(point * frequency + settings.center)); // 1-abs(v) squared gives a more ridgelike graph
            v *= v;
            v *= weight; // more detail on higher reigons
            weight = Mathf.Clamp01(v * settings.weightMultiplier);
            noiseValue += v * amplitude;
            frequency *= settings.roughness;
            amplitude *= settings.persistence;
        }

        noiseValue = noiseValue-settings.minValue;

        return noiseValue * settings.strength;
    }
}
