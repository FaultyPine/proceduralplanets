using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jup_Terrain_Scroll : MonoBehaviour
{
    [SerializeField]
    float scroll_speed = 1f;
    [SerializeField]
    Vector3 scroll_dir = new Vector3 (0f, 1f, 0f);

    bool should_animate = false;

    ShapeSettings shapeSettings;
    Planet Jup_Planet;
    Vector3 scroll;

    void Start()
    {
        scroll = scroll_dir * (scroll_speed / 60);
        Jup_Planet = GetComponent<Planet>();
        shapeSettings = Jup_Planet.shapeSettings;
    }

    void Update()
    {
        if (should_animate) {
            foreach (ShapeSettings.NoiseLayer noiseLayer in shapeSettings.noiseLayers) {
                NoiseSettings.FilterType filterType = noiseLayer.noiseSettings.filterType;
                if (filterType == NoiseSettings.FilterType.Simple) {
                    noiseLayer.noiseSettings.simpleNoiseSettings.center += scroll ;
                }
                else { // FilterType.Rigid
                    noiseLayer.noiseSettings.rigidNoiseSettings.center += scroll;
                }
            }
            Jup_Planet.MarkSettingsDirty();
        }
    }

    public void SetShouldAnimate(bool should) {
        should_animate = should;
    }
}
