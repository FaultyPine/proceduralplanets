using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
First, we get a basic cube.
Then normalize the distance of every vertex 
from a  point in the middle of the cube.
This pulls in points that are farther than a certain 
distance, and pushes other points out so they are all
in a perfect sphere.

We do this to get a sphere with evenly distributed verticies
since the default sphere isn't as evenly distributed.
*/

public class Planet : MonoBehaviour
{
    [Range(2,256)] // since 256 squared is about the max amount of verts a mesh can have
    public int resolution = 10;

    public bool autoUpdate = true;

    public enum FaceRenderMask {All, Top, Bottom, Left, Right, Front, Back};
    public FaceRenderMask faceRenderMask;

    public ShapeSettings shapeSettings;
    public ColorSettings colorSettings;

    [HideInInspector]
    public bool shapeSettingsFoldout;
    [HideInInspector]
    public bool colorSettingsFoldout;

    ShapeGenerator shapeGenerator = new ShapeGenerator();
    ColorGenerator colorGenerator = new ColorGenerator();

    [SerializeField, HideInInspector]
    MeshFilter[] meshFilters;
    TerrainFace[] terrainFaces;

    [HideInInspector]
    public bool isSettingsDirty = false;

    private void Start() {
        GeneratePlanet();
    }

    private void OnValidate() {
        GeneratePlanet();
    }

    void Update() {
        if (isSettingsDirty) {
            GeneratePlanet();
            isSettingsDirty = false;
        }
    }

    public void MarkSettingsDirty() {
        isSettingsDirty = true;
    }

    void Initialize() {
        colorGenerator.UpdateSettings(colorSettings);
        shapeGenerator.UpdateSettings(shapeSettings);
        if (meshFilters == null || meshFilters.Length == 0) {
            meshFilters = new MeshFilter[6];
        }
        terrainFaces = new TerrainFace[6];

        Vector3[] directions = {Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back};

        for (int i = 0; i < 6; i++) {
            if (meshFilters[i] == null) {
                GameObject meshObj = new GameObject("mesh");
                meshObj.transform.parent = transform;

                meshObj.AddComponent<MeshRenderer>();
                meshFilters[i] = meshObj.AddComponent<MeshFilter>();
                meshFilters[i].sharedMesh = new Mesh();

            }
            MeshCollider mc = this.transform.GetChild(i).GetComponent<MeshCollider>();
            if (mc == null)
                this.transform.GetChild(i).gameObject.AddComponent<MeshCollider>();

            meshFilters[i].GetComponent<MeshRenderer>().sharedMaterial = colorSettings.planetMaterial;

            terrainFaces[i] = new TerrainFace(shapeGenerator, meshFilters[i].sharedMesh, resolution, directions[i]);
            bool renderFace = faceRenderMask == FaceRenderMask.All || (int)faceRenderMask-1 == i;
            meshFilters[i].gameObject.SetActive(renderFace);
        }
    }

    // generate all aspects of planet
    public void GeneratePlanet() {
        Initialize();
        GenerateMesh();
        GenerateColors();
    }

    public void OnShapeSettingsChanged() {
        if (autoUpdate) {
            Initialize();
            GenerateMesh();
        }
    }

    public void OnColorSettingsUpdated() {
        if (autoUpdate) {
            Initialize();
            GenerateColors();
        }
    }

    void GenerateMesh() {
        for (int i = 0; i < 6; i ++) {
            if (meshFilters[i].gameObject.activeSelf) {
                terrainFaces[i].ConstructMesh();
            }
        }

        colorGenerator.UpdateElevation(shapeGenerator.elevationMinMax);
    }



    void GenerateColors() {
        colorGenerator.UpdateColors();
        for (int i = 0; i < 6; i ++) {
            if (meshFilters[i].gameObject.activeSelf) {
                terrainFaces[i].UpdateUVs(colorGenerator);
            }
        }
    }


}
