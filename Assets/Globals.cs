using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public const float gravitationalConstant = 0.0001f;
    public const float physicsTimeStep = 0.01f;
}
