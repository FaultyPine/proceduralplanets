using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSimulation : MonoBehaviour
{

    PlanetBody[] bodies;
    static PlanetSimulation self;
    [Range(1, 25)]
    public float timeScale = 1f;

    // populate bodies and set our physics timestep
    private void Awake() {
        bodies = FindObjectsOfType<PlanetBody>();
        Time.fixedDeltaTime = Globals.physicsTimeStep;
    }

    private void OnValidate() {
        UpdateTimeScale(timeScale);
    }

    public void UpdateTimeScale(float newTimeScale) {
        Time.timeScale = newTimeScale;
    }

    private void FixedUpdate() {
        // First, update planets velocities
        for (int i = 0; i < bodies.Length; i++) {
            Vector3 acceleration = CalculateAcceleration (bodies[i].Position, bodies[i]);
            bodies[i].UpdateVelocity (acceleration, Globals.physicsTimeStep);
            //bodies[i].UpdateVelocity (bodies, Globals.physicsTimeStep);
        }

        // then, ACTUALLY move the planets based on their velocities
        for (int i = 0; i < bodies.Length; i++) {
            bodies[i].UpdatePosition (Globals.physicsTimeStep);
            //bodies[i].UpdateRotation (Globals.physicsTimeStep); // this prevents planets from actually moving... for some reason (it moves in worldspace so why would rotation effect it??)
        }
    }


    // calculate acceleration for this planet based on all other planet's gravitational pull
    public static Vector3 CalculateAcceleration (Vector3 point, PlanetBody ignoreBody = null) {
        Vector3 acceleration = Vector3.zero;
        foreach (var body in Self.bodies) {
            if (body != ignoreBody) {
                float sqrDst = (body.Position - point).sqrMagnitude;
                Vector3 forceDir = (body.Position - point).normalized;
                                // F = G * ((m1*m2)/(dist^2))
                float accel_value = Globals.gravitationalConstant * body.mass / sqrDst;
                accel_value = Mathf.Clamp(accel_value, 0f, 100f);
                acceleration += forceDir * accel_value;
            }
        }
        return acceleration;
    }

    public static PlanetBody[] Bodies {
        get {
            return Self.bodies;
        }
    }

    static PlanetSimulation Self {
        get {
            if (self == null) {
                self = FindObjectOfType<PlanetSimulation> ();
            }
            return self;
        }
    }
}
