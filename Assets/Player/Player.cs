using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    Rigidbody rb;
    Transform closestPlanetTf;
    PlanetBody closestPlanetBody;

    public float speed;
    public float jetPackVertForce;
    public float mouseSensitivity;
    public float rotationTorque;

    public AudioSource jetpackAudio;
    public Text currentVelocity;
    public Text currentPlanet;

    public Transform planetsHolder;
    public Transform PlayerCam;

    float xRotation = 0f;
    float yRotation = 0f;
    //float zRotation = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        StartCoroutine(UpdateClosestPlanetCoroutine());
        StartCoroutine(UpdateVelocityText());
    }
    IEnumerator UpdateClosestPlanetCoroutine() {
        while (true) {
            UpdateClosestPlanet();
            // update every few seconds
            yield return new WaitForSeconds(3.0f);
        }
    }
    IEnumerator UpdateVelocityText() {
        while (true) {
            currentVelocity.text = "Current Velocity: " + ((int)rb.velocity.magnitude).ToString() + " U/s";
            yield return new WaitForSeconds(0.1f);
        }
    }


    void Update()
    {
        int isSpacePressed = Input.GetKey(KeyCode.Space) ? 1 : 0;
        int isShiftPressed = Input.GetKey(KeyCode.LeftShift) ? 1 : 0;
        //int isEPressed = Input.GetKey(KeyCode.E) ? 1 : 0;
        //int isQPressed = Input.GetKey(KeyCode.Q) ? 1 : 0;

        float vert = Input.GetAxis("Vertical");
        float horz = Input.GetAxis("Horizontal");
        float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * Time.deltaTime * mouseSensitivity;

        if (isSpacePressed-isShiftPressed != 0 || vert != 0 || horz != 0) {
            if (!jetpackAudio.isPlaying)
                jetpackAudio.Play();
        }
        else {
            jetpackAudio.Stop();
        }

        xRotation -= mouseY;

        yRotation += mouseX;

        //zRotation += isQPressed-isEPressed * rotationTorque;

        Quaternion rot = Quaternion.Euler(xRotation, yRotation, 0f);

        PlayerCam.localRotation = rot;
        this.transform.localRotation = rot;
        PlayerCam.position = this.transform.position;

        Vector3 moveForce = transform.right * horz + transform.forward * vert;
        int jetPackVal = isSpacePressed-isShiftPressed;
        moveForce.y = jetPackVal * jetPackVertForce;

        rb.AddForce(moveForce, ForceMode.Impulse);

        ApplyPlanetGravity();
    }

    void ApplyPlanetGravity() {
        Vector3 playerToPlanet = this.transform.position - closestPlanetTf.position;
        float playerToPlanetDist = playerToPlanet.magnitude;
        rb.AddForce(-playerToPlanet.normalized * playerToPlanetDist * Time.deltaTime, ForceMode.Force);
    }

    void UpdateClosestPlanet() {
        Transform[] transforms = new Transform[planetsHolder.childCount];
        for (int i = 0; i < planetsHolder.childCount; i++) {
            transforms[i] = planetsHolder.transform.GetChild(i);
        }
        Vector3 min = -Vector3.one;
        foreach (Transform tf in transforms) {
            Vector3 dist = tf.position - this.transform.position;
            if (min == -Vector3.one || dist.magnitude < min.magnitude) {
                min = dist;
                closestPlanetTf = tf;
            }
        }
        //Debug.Log("Closest planet: " + closestPlanetTf.gameObject.name);
        this.transform.parent = closestPlanetTf;
        closestPlanetBody = closestPlanetTf.gameObject.GetComponent<PlanetBody>();
        currentPlanet.text = "Current Planet: " + closestPlanetTf.gameObject.name;
    }

    public void ToggleFreezePlayer() {
        rb.isKinematic = !rb.isKinematic;
    }
    public void ToggleSyncWithClosestPlanet() {
        if (this.transform.parent == null)
            this.transform.parent = closestPlanetTf;
        else
            this.transform.parent = null;
    }

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "Sun") {
            Debug.Log("You (should have) died!     :)");
        }
    }
}
