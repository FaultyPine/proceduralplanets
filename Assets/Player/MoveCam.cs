using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCam : MonoBehaviour
{
    public float distScale;
    Transform PlanetParent;
    public Transform planetsHolder;

    void Start()
    {
        PlanetParent = this.transform.parent;
    }

    void Update()
    {
        float mouse_x = Input.GetAxis("Mouse X");
        float mouse_y = Input.GetAxis("Mouse Y");
        if (Input.GetKeyDown(KeyCode.Space)) {
            RandomizePosition();
        }
    }


    public void RandomizePosition() {
        Vector3 pointOnUnitSphere = Random.onUnitSphere;
        transform.position = PlanetParent.position + (pointOnUnitSphere * distScale);
        transform.LookAt(PlanetParent);
    }

    public void SetDistScale(float distScale) {
        this.distScale = distScale;
    }

    public void CamToTop() {
        transform.position = PlanetParent.position + (Vector3.up * distScale);
        transform.LookAt(PlanetParent);
    }
    public void SetNewParent(int idx) {
        for (int i = 0; i < planetsHolder.childCount; i++) {
            if (idx == i) {
                Transform planetTf = planetsHolder.GetChild(i);
                PlanetParent = planetTf;
                this.transform.parent = planetTf;
                CamToTop();
            }
        }
    }
}
