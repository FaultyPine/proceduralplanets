using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSelection : MonoBehaviour
{
    public Camera PlayerCam;
    public Camera PlanetCam;
    public Player player;
    public GameObject PlanetCamUIHolder;
    public GameObject PlayerCamUIHolder;

    public Text promptText;
    enum CameraMode {
        PLAYER,
        PLANET
    };
    CameraMode mode = CameraMode.PLAYER;


    public void SwitchCameras() {
        if (mode == CameraMode.PLAYER) { // player -> planet
            PlayerCam.gameObject.SetActive(false);
            PlanetCam.gameObject.SetActive(true);

            PlayerCamUIHolder.SetActive(false);
            PlanetCamUIHolder.SetActive(true);

            promptText.text = "Press P to switch to Player Cam";

            mode = CameraMode.PLANET;
            Cursor.lockState = CursorLockMode.None;
        }
        else { // planet -> player
            PlayerCam.gameObject.SetActive(true);
            PlanetCam.gameObject.SetActive(false);

            PlayerCamUIHolder.SetActive(true);
            PlanetCamUIHolder.SetActive(false);

            promptText.text = "Press P to switch to Planet Cam";

            mode = CameraMode.PLAYER;
            Cursor.lockState = CursorLockMode.Locked;
        }
        player.ToggleFreezePlayer();
        player.ToggleSyncWithClosestPlanet();
    }

    void Start() {
        PlayerCam.gameObject.SetActive(true);
        PlanetCam.gameObject.SetActive(false);

        PlayerCamUIHolder.SetActive(true);
        PlanetCamUIHolder.SetActive(false);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            SwitchCameras();
        }
    }

}
